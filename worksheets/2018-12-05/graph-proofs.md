# 5 December 2018, In-Class Activity

In today's class, we will practice proof techniques and communicating technical
ideas.

## Step 1  (16:10 - 16:30): Partner Up!

Please partner up into groups of size two.  The TAs will assign you one of
Problem A or Problem B.  You and your partner will work on proving the assigned
problem.  Take notes on scrap paper if needed.  At most one group should be more
than two people.  Write your name on the top of this page, and your partner(s) name(s) here:

### Problem A: 

Prove that a K_n has n(n-1)/2 edges.

Follow-up question if time allows): Prove that this is O(n^2).

### Problem B:

If a graph G=(V,E) is bipartite, then no cycle is of odd length.  (Here,
recall that a cycle is a circular list of edges such that the end of each edge 
is the start of the next).

Follow-up question (if time allows): Is the converse true?  Why or why not?

## Step 2 (16:30 - 16:40): Write-up

Now, sit far away from your partner.  And, write your solution to the problem on
the back of this page.  Be sure to put your name on the top of the page.

## Step 3 (16:40-16:55): Share Results

Find a new partner (here, we might need up to three groups of size three).  Now,
explain the proof that you just wrote down to your new partner (and vice versa).
You can add notes / remarks to your write-up if needed.  Write the name of your
new partner(s) here:

## Step 4 (16:55-17:00): Reflection

Answer the following question below: Did you find this activity helpful?  Why or why not?
