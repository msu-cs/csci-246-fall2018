# 12 November 2018, In-Class Activity

 Read Dr. Ernst's 
        notes on Loop Invariants. Keeping that in
        mind, we will prove correctness of a loop.

1. Write psuedo-code to find the first occurrence of a value, k, in a sorted
array.  Use a while-loop in your algorithm.
2.   What is the pre-condition and post-condition to the while loop? (We will
                refer to this pre-condition as $P$ and the post-condition as $Q$).
3.  What is the loop guard to the while loop? (We will
                refer to this as $X$).
4.   What is the loop invariant? (We will refer to this loop
                invariant as $L$).
5.   Show that $P \implies L$.
6.  Show that if $L$ is true when you begin an iteration of
                the while loop, then $L$ is true at the end of that iteration.
                (This is the induction part).
7.  Say, in words, what the following
                statement means: $\sim X \wedge L \implies Q$
                (This is called partial correctness).
8. Prove partial correctness.  
9. A decrementing function $D \colon \Z^+ \to Y$ is a
                function such that $Y$ is a well-ordered set, $D(i)$ is strictly
                decreasing, and $D(i)$ can be interpreted as a function value
                \emph{on the $i^{th}$ iteration of a loop}.  What are $Y$ and
                $D$ in the while loop?
10.  What is the minimum value of $Y$?
11.  Show that the loop will terminate in iteration $i$, where
                $D(i)$ is the minimum value of~$Y$.

. 


Repeat for the following:

1. Bubble sort.
2. Binary search.
