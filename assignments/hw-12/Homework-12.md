# CSCI 246, Week 14

This is the final set of Practice problems and homework problems for CSCI 246.
Good luck! 

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. 
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the final exam.

* Epp 9.4 (The Pigeonhole Principle): Exercises 1, 3, 5, 24-26, 31
* Epp 10.1 (Graphs: Definitions and Basic Properties): Exercises 1, 8, 14,
  17-24 
* Epp 10.2 (Trails, Paths, and Circuits): 1, 6, 12, 2, 32-35, 42.
* Epp 10.3 (Trees): Exercises 1-5, 25-28
* Additional reading: EF lectures 22-23; remainder of 4CT.

## Homework 12-REQ (the n+1st assignment)

This assingment is REQUIRED, and will count as one of your 10 homework grades.

The deadline for handing in this homework is the last day of class (7 December
2018) at 23:59.  We will accept late assignments with no penalty up until the
date and time of the Final exam.

Homework must be neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  

Write a two-page paper describing to me how you have grown as a computer 
scientist in this class and/or throughout this semester.  
To support your argument, you should include your 
homework and exams (or exceprts from your homework and exams) in an appendix as 
evidence (and reference them!)

If you don't feel that you've grown as a computer scientist, explain why 
(although, I hope that you will talk to me before you do this).

## Homework 12-OPT 
This homework can count towards one of the dropped homework grades (i.e., is
treated like a usual homework assignment).
The deadline for handing in this homework is Tuesday, 4 December 2018 at 23:59. 
No late assignments will be accepted.

Homework must be neatly typeset, and submitted as a PDF both to Gradescope and
to D2L by 23:59.  One point per question will be deducted if you do not select
the right page(s) for the problem in Gradescope. Don't forget to list your
collaborators at the beginning of each solution you submit, and to start each
question on a new page.

### Question 1 (5 Points)
The instructional team sincerely thanks you for all of the time and effort that
you put into this course.  We have worked hard to provide you with learnng
opportunies to build both essential prerequisite skills for courses going
forward (e.g., how to form a proof by induction and how to solve recurrence
relations) as well as to build life skills
you will likely need once you land a post-college job (e.g., how to write in
full sentences to communicate technical matters; how to use git). Now, it is
course evaluation time.  The numerical responses are for our bosses to see how
this class went; the free-response questions are for you to provide feedback to
us. In these, we encourage you to give constructive feedback.  We will welcome it
and will integrate changes the next time this course is taught.  

When filling
out the evaluation for this and for other classes, 
please remember the following from your student code of conduct:
``Students should follow fair and appropriate procedures when evaluating their
courses and instructors. Factors such as race, ethnicity, color, religion,
sex/gender, sexual orientation or preference, age, national origin, disability,
marital status, political beliefs, veteran status or personal relationships may
not be considered.``

To receive these five points, please just write the following sentence: I have
completed (or will complete) the course evaluation form for all courses I am 
taking this semester in a way that is fair and respectful.

### Question 2 (15 Points)
Epp Section 9.4, Exercise 30.

### Question 3 (20 Points)
Epp Section 10.1, Exercise 28.

### Question 4 (20 Points)
Epp Section 10.2, Exercise 36. 

### Question 5 (20 Points)
Epp Section 10.3, Exercise 29.

### Question 6 (20 Points)  
The second required book for this class, The Four Color Theorem by Wilson, has
quite a bit of material jam-packed into the small book.  Think about one topic 
that you learned about and found interesting or that you did not understand and
want to know better (from this book), and answer the following 5 questions:

1. What did the book tell you about this topic?

2. Why did you pick this topic?  If something you found interesting, explain why
   here.  If it is a question you have, state that question here.

3. Go online and find at least two resources to help you learn more about this
   topic.  Read them (and perhaps discuss with a TA or classmate your findings).
   Briefly, explain what you have learned.

4. Explain what additional questions or thoughts that you might now have as a
   result of your online investigation. 

5. Do you trust the resources that you found online when answerin part 3?  If
   yes, explain why?  If no, what source would you trust on this topic and how could you
   find it?

For each topic, please limit your response to at most two pages total.  The
responses are expected to be thougtful and complete.

### Extra Credit A (5 Points)
Create a table, with a caption, in LaTex.

### Extra Credit B (5 Points)
Name one thing you liked about this course and one thing you think could be
improved, and explain why for each.
