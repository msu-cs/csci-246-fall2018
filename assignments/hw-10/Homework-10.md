# CSCI 246, Week 12

The following practice and homework problems go along with the lectures from
Weeks 11 and 12 of the course.

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. 
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the final exam.

* Continue with problems from Sections 9.1-9.3 from the last assignment set.

## Homework Problems

The deadline for handing in this homework is Tuesday, 20 November 2018 at 23:59.
No late assignments will be accepted. 
In this homework assignment, you MUST write your solutions in groups of two to five students.

Homework must be neatly typeset, and submitted as a PDF both to Gradescope and to D2L by 23:59.
One point per question will be deducted if you do not select the right page(s) for the problem in Gradescope. Don't forget to list your collaborators at the beginning of each solution you submit.

### Question 1 (15 Points)
Section 9.1, Question 22

### Question 2 (25 Points)
Section 9.2, Question 17

### Question 3 (20 Points)
Section 9.3, Question 32

### Question 4 (30 Points)
Let $x$ be an integer and let $H$ be a hash function, $H: \Z \rightarrow \Z$, 
which computes an index for a hash table.
Let $H(x)$ be defined by $H(x)=x mod 25$.

4.1 Given two random integers $x$ and $y$,
chosen iid from the uniform distribution on the
set {1, 2, ..., 100}, what is the probability $H(x)=H(y)$?

4.2 What is the expected number of collisions when computing $H(x)$ for
ten integers chosen iid from the uniform distribution on the set {1,2,...,100}?

4.3 What is the expected number of integers we need to choose, iid from 
the uniform distrubition on the set {1,2,...,100}, and hash before we have
an element located at each index of our hash table?

### Question 5 (10 Points)
Choose one of the following people and explain _in your own words_ why they are
important in the history of computer science.  If you use external resources,
please provide proper citations.

* Daniel Bernoulli
* Thomas Bayes
* Vladimir Vapnik

### Extra Credit (5 Points)

Write one half page explaining how probability and counting plays an important
role in computer science.
