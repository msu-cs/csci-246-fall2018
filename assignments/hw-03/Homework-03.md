# CSCI 246, Week 3

The following practice and homework problems go along with the lectures from
Week 3 of the course (starting 10 September 2018).

## Practice Problems

The following problems provide practice for you to master this week's
topics. You will have time in class to work on these problems on Friday, 14
September 2018.  In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the exams.

* Section 8.1 (Relations on Sets), Problems 2, 3, 10, 13, 19.
* Section 8.2 (Reflexivity, Symmetry, and Transitivity), Problems 1-8, 12, 20,
  25, 34-36.
* Section 8.3 (Equivalence Relations), Problems 1-3, 8, 16, 22, 30-35.
* Section 2.1 (Logical Form and Logical Equivalence), Problems 6-9, 12-19, 25.
* Section 2.2 (Conditional Statements), Problems 5-11, 40-44.
* Section 2.3 (Valid and Invalid Arguments), Problems 1-10, 26-27, 37.
* Section 4.5 (Indirect Argument: Contradiction and Contraposition), Problems 1,
  18, 30.
* Section 4.5 (Indirect Argument: Two Classical Theorems), Problems 32-35.
* Challenge Problems: Section 8.2, Problem 30; Section 8.3, Problem 37; Section
  4.6, Problem 31.

## Homework Problems

The deadline for handing in this homework is Tuesday, 18 September 2018 at 23:59.
No late assignments will be accepted.

Homework must be  neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**

### Question 1 (10 Points)

Section 8.1, Problem 20.

### Question 2 (15 Points)

Section 8.2, Problem 21.

### Question 3 (15 Points)

Section 8.3, Problem 23.

### Question 4 (20 Points)

Section 2.1, Problems 20 and 22.

### Question 5 (10 Points)

Section 2.3, Problem 40.

### Question 6 (20 Points)

Section 4.5, Problem 29.

### Question 7 (10 Points)

Choose one of the following people and explain _in your own words_ why they are
important in the history of computer science.  If you use external resources,
please provide proper citations.

* Aristotle
* Augustus De Morgan
* Carl Frederich Gauss 

### Bonus Question (5 Points)

Use the LaTex [hyperref package](https://en.wikibooks.org/wiki/LaTeX/Hyperlinks)
to add a link to an external Resource (e.g., a Wikipedia article) by adding **a
footnote** that includes a URL **using the command** `\ul{LINK}`, where LINK is the
full URL to your resurce.

Note: Remember that we require you to list all resources at the TOP of a page.
So, if you will be using in text footnotes, please add the following line to the
top of the problem that uses a footnote reference: `References to online
resources are provided as footnotes.`

