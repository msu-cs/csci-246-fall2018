# CSCI 246, Week 4

The following practice and homework problems go along with the lectures from
Weeks 4 and 5 of the course.

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. You will have time in class to work on these problems on 21 and 28 September 2018.
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the exams.

* Section 3.1 (Predicates and Quantified Statements I), Problems 1-5, 13, 16,
  26-27.
* Section 3.2 (Predicates and Quantified Statements II), Problems 2-3, 9-10,
  26-33, 36, 43-45.
* Section 3.3 (Statements with Multiple Quantifiers), Problems 11, 13-15, 46-54.
* Section 3.4 (Arguments with Quantified Statements), Problems 7-16, 31, 33.
* Big-O notation: Handout - will post shortly.

## Homework Problems

The deadline for handing in this homework is Tuesday, 2 October 2018 at 23:59.
No late assignments will be accepted.

Homework must be  neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**

### Question 1 (15 Points)

Section 3.1, Problem 29. 

### Question 2 (5 Points)

Section 3.2, Problem 38.

### Question 3 (10 Points)

Section 3.2, Problem 47.

### Question 4 (6 Points)

Section 3.4, Problem 34.

### Question 5 (18 Points)

Does big-O define an equivalence relation (reflexive, symmetric, transitive)?  
For the properties  it satisfies,
prove it.  For the properties it does not satisfy, explain why that property is
not satisfied.

### Question 6 (16 Points) 

Prove or disprove each of the following statements:

a. The function f(x) = 2x^2 is O( 4x ).

b. The function g(x) = 3x is \Omega(x).

c. The function h(x) = x^2 + \log x  is O (x^2).

d. The fnction k(x) = 5x^2 + x is \Theta(x).

### Question 7 (20 Points)

a. Consider the map of the continental US on Page 5.  Why can we color Utah and New Mexico
the same color, even though the two states have a common vertex?

b. Again, looking at the map of the continental US on Page 5, explain why
Michigan does not satisfy the conditions for the four color theorem.

c. Explain why we an omit the states of Hawaii and Alaska in order to construct
a four-coloring of the states in the USA.

d. Is the following statement TRUE or FALSE?  Explain. _Four colors are
necessary to color all maps_.

### Question 8 (10 Points)

Choose one of the following people and explain _in your own words_ why they are
important in the history of computer science.  If you use external resources,
please provide proper citations.  **If you do not use external resources, please
state so explicitly.**

* Charles Babbage 
* Charles Sanders Peirce
* Alfred Tarski

### Bonus Question A (5 Points)

In LaTex, you can define environments to help format.  In the style file
fasy-hw.sty, we have defined a `proof` environment for you.  to use it, begin a
proof with `\begin{proof}` and end it with `\end{proof}`.  Use this proof
environment to format at least one of your proofs in this homework.  You should
select the page containing the proof in Gradescope.

### Bonus Question B (5 Points)
Explain one application of the four color theorem that does not involve coloring
geographic maps.
