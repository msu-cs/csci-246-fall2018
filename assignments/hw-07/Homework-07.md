# CSCI 246, Week 6

The following practice and homework problems go along with the lectures from
Week 8 of the course.

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. 
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the exams.

* Epp 8.5 (The Euclidean Algorithm and Applications): Problems 6, 9, 12, 13, 27,
  29.
* Additional reading: EF lectures 4 and 5; 4CT Ch. 5.

## Homework Problems

The deadline for handing in this homework is Tuesday, 23 October 2018 at 23:59.
No late assignments will be accepted. 

Homework must be  neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**
Collaborating on homework is encouraged, but individual solutions must be
submitted.

### Question 1 (20 Points)
Section 8.5, Problem 14.

### Question 2 (10 Points)
Section 8.5, Problem 30.

### Question 3 (20 Points)

3.1 What is gcd(91,42)? 

3.2 What is lcm(37,15)?  (Note: lcm was defined in Exercise Set 8.5 of the text,
right before practice problem 12).

### Question 4 (20 Points)  
What are the additive and multiplicitive inverses of the elements of Z_9 and
Z_11 (where the inverses exist). Show the addition and multiplication tables for
each.

### Question 5 (20 Points)

For the following, draw a map (with coloring or colorings) 
that has at least 5 faces and satisfies the following:

5.1 Has a two-coloring.  Show the coloring.

5.2 Has a three-coloring, but not a two-coloring.

5.3 Has a four-coloring, but not a three-coloring. 

5.4 Has two different colorings (that are not simply a relabeling of the
same coloring).

Examples cannot be the same as given in the book, if examples are given.

### Question 5 (10 Points)
Choose one of the following people and explain _in your own words_ why they are
important in the history of computer science.  If you use external resources,
please provide proper citations.

* Fran Allen
* Shafi Goldwasser
* Leslie Gabriel Valiant

