# CSCI 246, Homework 02

The deadline for handing in this homework is Tuesday, 11 September 2018 at 23:59.
No late assignments will be accepted.

Homework must be  neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**

## Question 1 (20 Points)

Section 4.3, Problems 33 and 34.

## Question 2 (10 Points)

Section 4.4, Problem 15.  For full credit, all work must be shown.

## Question 3 (20 Points)

Section 4.4, Problem 35.

## Question 4 (15 Points)

Section 9.2, Problem 10.

## Question 5 (10 Points)

Section 9.6, Problem 7.

## Question 6 (15 Points)

Section 9.6, Problem 32.

## Question 7 (10 Points)

Choose one of the following people and explain _in your own words_ why they are
important in the history of computer science.  If you use external resources,
please provide proper citations.

* Ada Lovelace
* Blaise Pascal

## Bonus Question (5 Points)

Use the `figure` environment to add a figure, with a caption to your homework
(The sketch you use for Question 4 would be a great choice of where to add a
figure).  Your figure can be hand drawn and scanned, or can be made using a tool
such as Inkscape.

------------------

## Friday, 7 September 2018

During the problem session on Friday, 7 September, you will work in groups of
size two to four students to discuss the additional problems posted below.  You
may choose your own groups this week.

## Additional Problems

The following problems provide additional practice for you to master this week's
topics.  We are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the exams.

1.  Prove: `$\sum_{i=0}^n {n}\choose{i} = 2^n$`
![corollary](https://latex.codecogs.com/gif.latex?$\sum_{i=0}^n&space;{{n}\choose{i}}&space;=&space;2^n$)
Hint: Use the Binomial Theorem.

2. Prove that the following two definitions of `even` are the same: (a) An integer x is even if x%2=0; (b) An integer x is even if there exists an integer k such that x=2k.  

3. Section 4.3, Problems 10-15, 19, 22, 32.

4. Section 4.4, Problesm 1, 13-14, 23, 28, 44.

5. Section 9.2, Problems 11, 13, 22

6. Section 9.6, Problems 1-6, 19-29, 43. 

7. Challenge questions: Section 4.3, Problem 48; Section 9.2, Problem 25.
