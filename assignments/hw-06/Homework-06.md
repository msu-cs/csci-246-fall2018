# CSCI 246, Week 6

The following practice and homework problems go along with the lectures from
Week 6 of the course.

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. 
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the exams.

* Additional reading: EF lectures 11 and 12; 4CT Ch. 3.
* Epp 5.5 (Defining Sequences Recursively): Problems 1-5, 11, 15, 19, 31, 40.
* Epp 5.6 (Solving Recurrence Relations by Iteration): Problems 1, 3-6, 18-19 
* Additional reading: EF lectures 4 and 5; 4CT Ch. 4.

## Homework Problems

The deadline for handing in this homework is Tuesday, 16 October 2018 at 23:59.
No late assignments will be accepted.  **In this homework assignment, you MUST write your solutions in groups of two to five students.**

Homework must be  neatly typeset,
and submitted as a PDF both to Gradescope and to D2L by 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**

### Question 1 (20 Points)

1.1 Prove that f(x) = 2^x + x^2 is O(4^x).
1.2 Is it true that g(x) = 4^x + x is O(f(x))? Prove it.

### Question 2 (15 Points)
The Fibonacci numbers are defined as follows: F_0=F_1=1. F_n=F_{n-1} + F_{n-2}
for all n \geq 2.  Prove \Sum_{i=0}^n F_i = F_{n+2}-1. 

### Question 3 (20 Points)
Section 5.5, Problem 42.

### Question 4 (20 Points)  
Section 5.5, Problems 7 and 32. 

### Question 5 (20 Points)  
In the Four Colors Suffice book, we saw the definition of Euler's Formula for a
finite decomposition of a Sphere or 2-plane into vertices, edges, and faces.

5.1 What is the other formula known as Euler's formula?

5.2 Consider the following construction: Start with a solid cube.  Then, slice
off a small region around eachvertex (image you have a sharp knife, so you take
off a tetrahedron at each corner).  How many vertices, edges, and faces are on
the surface of this object before and after this operation? What polyhedron is this?

5.3 Draw a projection of the octahedron onto the plane such that edges only
intersect at vertics.  Can every polyhedron be drawn in such a way?

5.4 If we have a tree T=(V,E), what is |V|-|E|?  Be sure to prove your claim. 

### Question 6 (5 Points)
Choose one of the following people and explain _in your own words_ why they are
important in the history of computer science.  If you use external resources,
please provide proper citations.

* Augustin-Louis Cauchy
* Euclid
* Leonhard Euler
* Fibonacci (Leonardo of Pisa)
* Christian Goldbach
* Richard Stallman

### Bonus Question A (5 Points)

In LaTex, use super-scrips and sub-scripts in math mode in one of the solutions
to a homework problem.

### Bonus Question B (5 Points)

One thing that we need to consider as computer scientists is making our products
(software, technical papers) accessible to a wide range of people. When
designing GUIs or writing technical papers (e.g., journal papers or even
homework solutions), explain five things that you could do to make your product
more accessible to people who might be colorblind or colorweak (or have a bad
computer screen).
