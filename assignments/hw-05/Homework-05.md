# CSCI 246, Week 6

The following practice and homework problems go along with the lectures from
Week 6 of the course.

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. 
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the exams.

* Epp 5.1 (Sequences): 6-10, 14-15, 32, 48.
* Epp 5.2 (Mathematical Induction I): 1, 3, 5, 8, 10, 15, 33-34.
* Epp 5.3 (Mathematical Induction II): 3-4, 6, 8, 11, 30-32.
* Epp 5.4 (Strong Mathematical Induction): 1, 11, 16.
* Additional reading: EF lectures 11 and 12; 4CT Ch. 3.

## Homework Problems

The deadline for handing in this homework is Tuesday, 9 October 2018 at 23:59.
No late assignments will be accepted.  ** In this homework assignment, you may write your solutions in groups of up to five students.**

Homework must be  neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**

### Question 1 (20 Points)
Section 5.1, Problem 49.

### Question 2 (20 Points)
Section 5.2, Problem 16.

### Question 3 (20 Points)
Section 5.3, Problem 7.

### Question 4 (20 Points)  
Section 5.4, Problem 25.

### Question 5 (20 Points)
These 20 points are a gift, since this homework assignment was late to be posted.

### Bonus Question (5 Points)

Look through proofs in this textbook, or other books / papers.  Define five qualities that you think are common among good proofs.
