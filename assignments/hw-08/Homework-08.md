# CSCI 246, Weeks 9-10

The following practice and homework problems go along with the lectures from
Weeks 9 and 10 of the course.

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. 
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the exams.

* Epp 8.4 (Modular Arithmetic): Problems 1, 34.
* Epp 9.1 (Intro to Counting and Probability): Problems 2, 3, 7, 16, 18, 21.
* Epp 9.2 (Possibility Trees and the Multiplication Rule): Problems 1, 6, 9, 19,
  23.
* Epp 9.3 (Counting Elements of Disjoint Sets: The Addition Rule): Problems 3-7,
  12-15, 24-25.
* Additional reading: EF lectures 15; 4CT Ch. 6-7.

## Homework Problems

The deadline for handing in this homework is Tuesday, 6 November 2018 at 23:59.
No late assignments will be accepted. 

Homework must be neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**
Collaborating on homework is encouraged, but solutions that you hand in must be
in your own words.

### Question 1 (20 Points)
Use induction to prove that a tree always has one more vertex than edges.

### Question 2 (20 Points)
Use Master's theorem to solve the following recurrence relations.  If you use
cases one or three, be sure to state what \epsilon should be.

###2.1 
T(n) = 9 T(n/3) + n

###2.2 
T(n) = T(n/2) + 1

### Question 3 (20 Points)
The following is an ecrypted message: DUR OR MNSG. 

(a) What is the decoded message; (b) How
did you determine what the message was? Explain what you tried, both the
successsful, and the unsuccessful attempts, if applicable.  If known, explain
what the encryption key is.

### Question 4 (20 Points)  
Section 9.3, Problem 30 (the birthday problem).

### Question 5 (20 Points)
What is the dual graph of Example 1 (page 9 of Wilson)?

### Question 6 (5 Points)
Choose one of the following people and explain _in your own words_ why they are
important in the history of computer science.  If you use external resources,
please provide proper citations.

* Grace Hopper
* Michael Jordan (not the basketball star)
* Alfred Bray Kempe
* Pierre-Simon Laplace

### Extra Credit (5 Points)
Choose a faculty member in the School of Computing at MSU.  Write a short (0.5
to one-page) biograph of them, using publicly available information
(e.g., their website, Google scholar, Researchgate).  Things you may want to
discuss are: where they went to school, a short summary of their most-cited
paper, some of their research and teaching interests.
