# CSCI 246, Week 13

The following practice and homework problems go along with the lectures through Week 13 of the course.

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. 
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the final exam.

* Continue with problems from Sections 9.1-9.3 from the last assignment set.
* Additional reading: EF lectures 16-17; 4CT Ch. 8.

## Homework Problems

The deadline for handing in this homework is Wednesday, 28 November 2018 at 23:59.
No late assignments will be accepted. 

Homework must be neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**
Collaborating on homework is encouraged, but solutions that you hand in must be
in your own words.

### Question 1 (50 Points)
Happy Thanksgiving!

### Question 2 (25 Points)
Compute the probability distribution, expectation, and variance of the following
random variables:

####1.1 
The number of heads when flipping a top-heavy coin three times, where the probability of
flipping a head is 75%.  (Heads = H = 1; Tails = T = 0).

####1.2 
Multipliying the result of rolling two dice.

### Question 3 (10 Points)
Start with the plane and draw n straight lines on that plane.  Prove, in your own
words, that it is
possible to two-color this map. 

### Question 4 (10 Points)
Use Master's Theorem to solve the following recurrence.  If you use Case 1 or 3,
be sure to state what epsilon is:

####1.1 
T(n) = 2T(n/4) + \log n

####1.1 
T(n) = 5 T(n/5) + n/3

### Question 5 (5 Points)  
Explain, in your own words, why Master's Theorem is important.
