# CSCI 246, Week 11 

The following practice and homework problems go along with the lectures from
Weeks 10 and 11 of the course.

## Practice Problems

The following problems provide practice for you to master this weeks'
topics. 
In addition, we are happy to discuss solutions to these problems either on Piazza or
in office hours.  Working through these problems is also a great starting point
for studying for the final exam.

* Continue with problems from Sections 9.1-9.3 from the last assignment set.
* Additional reading: EF lectures 16-17; 4CT Ch. 8.

## Homework Problems

The deadline for handing in this homework is Tuesday, 13 November 2018 at 23:59.
No late assignments will be accepted. 

Homework must be neatly typeset,
and submitted as a PDF both to Gradescope and to D2Lby 23:59.  
One point per question will be deducted if you do not select the right page(s)
for the problem in Gradescope.
**Don't
forget to list your collaborators at the beginning of each solution you submit.**
Collaborating on homework is encouraged, but solutions that you hand in must be
in your own words.

### Question 1 (25 Points)
Suppose you have a map that consists of six vertical strips.  
So, each region has two
neighbors: one left and one right (except the leftmost region does not have a
left neighbor and the rightmost region does not have a right neighbor.  If we
have five possible colors, and paint each region randomly, with equal
probability of each color:

####1.1 
What is the probability that the coloring is a four-coloring?

####1.2 
What is the expected number of colors that you will use?

### Question 2 (25 Points)
Section 9.1, Question 20.

### Question 3 (25 Points)
Section 9.2, Question 7.

### Question 4 (15 Points)  
Section 9.3, Question 16. 

Note: I suggest doing Section 9.3, Quesion 2 first.

### Question 5 (10 Points)
Choose one of the following people and explain _in your own words_ why they are
important in the history of computer science.  If you use external resources,
please provide proper citations.

* Charles Ludwidge Dodgson
* Hedy Lamarr
* Barbara Liskov
* Lena S\"oderberg

### Extra Credit (5 Points)

What is the most important skill or concept that you have learned in a CS (or
related) class to date? Why?  In your own words, write a one-page response to
this question.
